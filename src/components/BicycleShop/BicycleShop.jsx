import React from "react";
import { Button, Container, Icon, Segment } from "semantic-ui-react";
import LatestOffer from "../LatestOffer/LatestOffer";
import MainMenu from "../MainMenu/MainMenu";

const BicycleShop = () => {
  return (
    <>
      <MainMenu />
      <Container style={{ padding: "60px 0 20px 0" }}>
        <LatestOffer />
      </Container>
      <div style={{ textAlign: "center" }}>
        <a href="https://gerardvanderput.com" target="_blank">
          <Button icon size="mini" labelPosition="left">
            <Icon name="heart" color="red" />
            Gerard van der Put.com
          </Button>
        </a>
      </div>
    </>
  );
};

export default BicycleShop;
