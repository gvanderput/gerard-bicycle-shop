import classnames from "classnames";
import React from "react";
import bicycleImage from "./bicycle.jpg";
import styles from "./LatestOffer.module.scss";

const LatestOffer = () => {
  return (
    <div className={styles.latestOffer}>
      <img src={bicycleImage} alt="Bicycle" />
      <div className={styles.banner}>
        <div className={styles.column}>
          <div className={classnames(styles.cell, styles.offer)}>Latest offer!</div>
          <div className={classnames(styles.cell, styles.shaded)}>The Jellybean 5000™</div>
        </div>
        <div className={classnames(styles.column, styles.price)}>
          <div className={classnames(styles.cell, styles.oldPrice)}>$ 1,999.00</div>
          <div className={classnames(styles.cell, styles.shaded)}>$ 1,599.00</div>
        </div>
      </div>
      <div className={styles.shadow} />
    </div>
  );
};

export default LatestOffer;
