import React from "react";
import { Container, Menu } from "semantic-ui-react";

export default function MainMenu() {
  return (
    <Menu fixed="top" inverted color="black" style={{ border: "5px solid white" }}>
      <Container fluid>
        <Menu.Item header>Gerard's Bicycle Shop</Menu.Item>
      </Container>
    </Menu>
  );
}
